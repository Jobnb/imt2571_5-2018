<?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */  
class XmlSkierLogs
{
    /**
      * @var DOMDocument The XML document holding the club and skier information.
      */  
    protected $doc;
    
    /**
      * @param string $url Name of the skier logs XML file.
      */  
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
    }
    
    /**
      * The function returns an array of Club objects - one for each
      * club in the XML file passed to the constructor.
      * @return Club[] The array of club objects
      */
    public function getClubs()
    {
        $clubs = array();
		
		$xpath = new DOMXpath($this->doc);
		$nodeList = $xpath->query('/SkierLogs/Clubs/Club');
        
		foreach($nodeList as $club) {
			$clubId = $club->getAttribute('id');
			
			$clubName = $club->getElementsByTagName('Name')->item(0)->nodeValue;
			$clubCity = $club->getElementsByTagName('City')->item(0)->nodeValue;
			$clubCounty = $club->getElementsByTagName('County')->item(0)->nodeValue;
			
			$clubTemp = new Club($clubId, $clubName, $clubCity, $clubCounty);
			array_push($clubs, $clubTemp);
		}
		
        return $clubs;
    }

    /**
      * The function returns an array of Skier objects - one for each
      * Skier in the XML file passed to the constructor. The skier objects
      * contains affiliation histories and logged yearly distances.
      * @return Skier[] The array of skier objects
      */
    public function getSkiers()
    {
        $skiers = array();
		
		$xpath = new DOMXpath($this->doc);
		$nodeList = $xpath->query('/SkierLogs/Skiers/Skier');
        
		foreach($nodeList as $skier) {
			$skierId = $skier->getAttribute('userName');
			$skierFName = $skier->getElementsByTagName('FirstName')->item(0)->nodeValue;
			$skierLName = $skier->getElementsByTagName('LastName')->item(0)->nodeValue;
			$skierBirth = $skier->getElementsByTagName('YearOfBirth')->item(0)->nodeValue;
			
			$skierTemp = new Skier($skierId, $skierFName, $skierLName, $skierBirth);
			
			
			//Affiliation
			$skierList = $xpath->query("/SkierLogs/Season/Skiers/Skier[@userName='$skierId']");
			
			foreach($skierList as $aff) {
				if($aff->parentNode->getAttribute('clubId')){
				$clubIdTemp = $aff->parentNode->getAttribute('clubId');
				$seasonTemp = $aff->parentNode->parentNode->getAttribute('fallYear');
					
				$skierTemp->addAffiliation(new Affiliation($clubIdTemp, $seasonTemp));
				}
			}
			
			
			//Distance		
			foreach($skierList as $dist) {
				$distCount = 0;
				$fallYearTemp = $dist->parentNode->parentNode->getAttribute('fallYear');
				
				foreach($dist->getElementsByTagName('Distance') as $singleDist){
					$distCount += $singleDist->nodeValue;
				}
					
				$skierTemp->addYearlyDistance(new YearlyDistance($fallYearTemp, $distCount));
			}
			
			array_push($skiers, $skierTemp);
		}
		
        return $skiers;
    }
}

?>